using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace CardGame
{
	public class TurnManager : MonoBehaviour
	{
		public static TurnManager Instance;

		[Header("Slot Boards")]
		[SerializeField] private SlotBoard m_playerBoard;
		[SerializeField] private SlotBoard m_aiBoard;

		[Header("Current phase")]
		/*[DisplayAsString]*/ public ETurnPhase CurrentPhase = ETurnPhase.InitializeManagerLoadsCards;

		private int m_turn = 0;

		private void Awake()
		{
			Instance = this;
		}

		private void Update()
		{
			switch(CurrentPhase)
			{
				case ETurnPhase.InitializeManagerLoadsCards:
					if (InitializeManager.Instance.LoadedCards == false)
						return;

					CurrentPhase = ETurnPhase.PlayerPlaysCard;
					break;
				case ETurnPhase.PlayerPlaysCard:
					///do nothing, wait for <see cref="OnPlayerPlayedCard(CardActor)"/>
					break;
				case ETurnPhase.PlayerDoesCardEffect:
					///do nothing, wait for <see cref="OnPlayerFinishedProcessingEffects()"/>
					break;
				case ETurnPhase.AIPlaysCard:
					///do nothing, wait for <see cref="OnAIFinishedMove"/> or
					///<see cref="OnCardPlacedOnBoard(CardActor)"/> to indicate that ai has effects to process
					break;
				case ETurnPhase.AIDoesCardEffect:
					AIManager.Instance.ProcessCardsEffectsAfterCardMovedOnBoard();
					OnAIFinishedMove();
					break;
				default:
					Debug.Log("unknown " + CurrentPhase, this.gameObject);
					break;
			}
		}

		public void OnPlayerPlayedCard(CardActor playedCard)
		{
			Assert.IsFalse(CurrentPhase != ETurnPhase.PlayerPlaysCard);

			if (playedCard.HasEffectsRequiringPlayerToClick)
			{
				//player starts processing effects
				CurrentPhase = ETurnPhase.PlayerDoesCardEffect;
				EffectManager.Instance.OnPlayerPlayedCard(playedCard);
			}
			else
				OnPlayerFinishedMove();
		}

		public void OnPlayerFinishedProcessingEffects()
		{
			Assert.IsFalse(CurrentPhase != ETurnPhase.PlayerDoesCardEffect);
			OnPlayerFinishedMove();
		}

		public void OnPlayerFinishedMove()
		{
			Assert.IsFalse(CurrentPhase != ETurnPhase.PlayerPlaysCard && CurrentPhase != ETurnPhase.PlayerDoesCardEffect);
			
			//ai starts
			CurrentPhase = ETurnPhase.AIPlaysCard;

			//if ai can start
			if(PlayerManager.Instance.CountOfHand <= 0 && AIManager.Instance.CountOfHand <= 0)
			{
				ShowEndScreen();
				return;//player and ai don't have cards
			}
			else if (AIManager.Instance.CountOfHand <= 0)
			{
				UIWindowsManager.Instance.PointsUI.SetPlayerPoints(m_playerBoard.GetPointsOnBoard());
				UIWindowsManager.Instance.PointsUI.SetAIPoints(m_aiBoard.GetPointsOnBoard());
				OnAIFinishedMove();
				return;//ai doesn't have cards > let player play
			}
			if (m_aiBoard.AreAllSlotsFull())
			{
				Debug.Log("all ai slots are full, add slots", this.gameObject);
				ShowEndScreen();
				return;//ai can't play card because its slots are full
			}

			AIManager.Instance.PlayNextCard();

			//update points
			UIWindowsManager.Instance.PointsUI.SetPlayerPoints(m_playerBoard.GetPointsOnBoard());
			UIWindowsManager.Instance.PointsUI.SetAIPoints(m_aiBoard.GetPointsOnBoard());
		}

		public void OnAIFinishedMove()
		{
			Assert.IsFalse(CurrentPhase != ETurnPhase.AIPlaysCard && CurrentPhase != ETurnPhase.AIDoesCardEffect);

			//next turn
			m_turn++;
			UIWindowsManager.Instance.TurnUI.SetTurn(m_turn);
			EffectManager.Instance.ChangeHealthOnTurnEnd();

			//player starts
			CurrentPhase = ETurnPhase.PlayerPlaysCard;

			//if player can start
			if (m_playerBoard.AreAllSlotsFull())
			{
				Debug.Log("all player slots are full, add slots", this.gameObject);
				ShowEndScreen();
				return;//player can't play card because its slots are full
			}
			if (PlayerManager.Instance.CountOfHand <= 0 && AIManager.Instance.CountOfHand <= 0)
			{
				ShowEndScreen();
				return;//player and ai don't have cards
			}
			else if (PlayerManager.Instance.CountOfHand <= 0)
			{
				//update points
				UIWindowsManager.Instance.PointsUI.SetPlayerPoints(m_playerBoard.GetPointsOnBoard());
				UIWindowsManager.Instance.PointsUI.SetAIPoints(m_aiBoard.GetPointsOnBoard());
				OnPlayerFinishedMove();
				return;//player doesn't have cards
			}

			//update points
			UIWindowsManager.Instance.PointsUI.SetPlayerPoints(m_playerBoard.GetPointsOnBoard());
			UIWindowsManager.Instance.PointsUI.SetAIPoints(m_aiBoard.GetPointsOnBoard());
		}

		private void ShowEndScreen()
		{
			//update UI
			UIWindowsManager.Instance.PointsUI.SetPlayerPoints(m_playerBoard.GetPointsOnBoard());
			UIWindowsManager.Instance.PointsUI.SetAIPoints(m_aiBoard.GetPointsOnBoard());
			UIWindowsManager.Instance.CardInfoUI.HideMe();
			UIWindowsManager.Instance.EffectUI.ClearClickCard();

			int playerPoints = m_playerBoard.GetPointsOnBoard();
			int aiPoints = m_aiBoard.GetPointsOnBoard();

			if(playerPoints > aiPoints)
				UIWindowsManager.Instance.VictoryUI.ShowVictoryScreen(VictoryOrDefeat.Victory);
			else if (aiPoints == playerPoints)
				UIWindowsManager.Instance.VictoryUI.ShowVictoryScreen(VictoryOrDefeat.Draw);
			else
				UIWindowsManager.Instance.VictoryUI.ShowVictoryScreen(VictoryOrDefeat.Defeat);
		}

		public void OnCardPlacedOnBoard(CardActor card)
		{
			if (CurrentPhase != ETurnPhase.AIPlaysCard)
				return;//player played this card

			if (AIManager.Instance.CardWithEffectToDoAfterMove == null)
			{
				OnAIFinishedMove();
				return;//ai doesn't have card effects to process
			}

			StopAllCoroutines();
			StartCoroutine(SetPhaseToAIDoesEffectAfterTime());
		}

		IEnumerator SetPhaseToAIDoesEffectAfterTime()
		{
			yield return new WaitForSeconds(.3f);//add a little delay after ai placed card on table, before ai does effects
			CurrentPhase = ETurnPhase.AIDoesCardEffect;
		}
	}



	public enum ETurnPhase
	{
		InitializeManagerLoadsCards = 4,
		PlayerPlaysCard = 0,
		AIPlaysCard = 1,
		AIDoesCardEffect = 2,
		PlayerDoesCardEffect = 3,
	}
}