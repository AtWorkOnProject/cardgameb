using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace CardGame
{
	/// <summary>
	/// loads all prefabs of cards from Prefab/Cards folder
	/// initializes player's and ai's hands
	/// </summary>
	public class InitializeManager : MonoBehaviour
	{
		public static InitializeManager Instance;

		[Header("Loading Cards")]
		[SerializeField] CardActor BaseCardPrefab;
		// this won't be included in possible cards. doesn't work in build so BaseCardData is moved out of CardsData Folder
		[SerializeField] CardDataSO BaseCardData;
		[SerializeField] List<CardDataSO> m_possibleCardData;

		/*[DisplayAsString]*/ public bool LoadedCards = false;

		[Header("Hands")]
		[SerializeField] float xSpaceBetweenCards = 1.5f;//was determined in play
		[SerializeField] Transform m_playerHand;
		[SerializeField] Transform m_aiHand;

		[Header("Debug")]
		[TextArea(2, 10)]
		[SerializeField] string DebugLog;


		private void Awake()
		{
			Instance = this;
		}

		private void Start()
		{
			ClearHands();
			Addressables.LoadAssetsAsync<CardDataSO>("CardsFolder", null).Completed += OnLoadDone;
		}

		private void OnLoadDone(
			UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<IList<CardDataSO>> obj
			)
		{
			DebugLog = "";

			if (obj.Result == null)
			{
				Debug.Log("couldn't load possible cards", this.gameObject);
				DebugLog += "ERROR\n";
				if (obj.OperationException != null)
				{
					Debug.Log(obj.OperationException.ToString(), this.gameObject);
					DebugLog += obj.OperationException.ToString() + "\n";
				}
				LoadedCards = true;//this will trigger instant draw
				return;
			}

			m_possibleCardData = new List<CardDataSO>();
			foreach(var cardData in obj.Result as List<CardDataSO>)
			{
				if (cardData == null)
					continue;
				if (cardData == BaseCardData)
					continue;
				if (cardData is CardDataSO == false)
					continue;
				DebugLog += "loaded " + cardData.Header + "\n";
				m_possibleCardData.Add(cardData);
			}

			//initialize hands on hand
			PlayerManager.Instance.SetHand(InitializeCards(m_playerHand));
			AIManager.Instance.SetHand(InitializeCards(m_aiHand));
			LoadedCards = true;
		}

		private void ClearHands()
		{
			foreach (var c in m_aiHand.GetComponentsInChildren<CardActor>())
				Destroy(c.gameObject);
			foreach (var c in m_playerHand.GetComponentsInChildren<CardActor>())
				Destroy(c.gameObject);
		}

		private List<CardActor> InitializeCards(Transform handTransform)
		{
			List<CardActor> returnedCards = new List<CardActor>();
			int numberOfCards = ConfigManager.Instance.Configuration.StartingCards;

			for (int i = 0; i < numberOfCards; i++)
			{
				var newCard = Instantiate(BaseCardPrefab, handTransform);

				//assign random card data
				var randomCardData = m_possibleCardData[Random.Range(0, m_possibleCardData.Count)];
				newCard.Data = randomCardData;

				returnedCards.Add(newCard);
			}

			float totalXSpace = (float)numberOfCards * xSpaceBetweenCards;
			Vector3 currentPlace = handTransform.position + (Vector3.left * (totalXSpace / 2.0f));
			foreach (CardActor card in returnedCards)
			{
				card.MoveMeTo_Instant(handTransform.position);
				card.MoveMeTo(currentPlace);
				currentPlace += Vector3.right * xSpaceBetweenCards;
			}

			return returnedCards;
		}
	}
}
