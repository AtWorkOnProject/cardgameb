using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace CardGame
{
	/// <summary>
	/// Dragging card logic, mouse clicking logic
	/// 
	/// todo maybe raycast2d with 2d colliders, but not quite needed for 16 card colliders in game
	/// </summary>
	public class MouseManager : MonoBehaviour
	{
		[Header("Player slot board")]
		[SerializeField] private SlotBoard m_playerBoard;

		[Header("Layers")]
		[SerializeField] private LayerMask m_cardLayer;
		[SerializeField] private LayerMask m_dragPlaneLayer;
		[SerializeField] private LayerMask m_slotLayer;

		[Header("Debug, don't use")]
		[SerializeField] private CardActor m_draggedCard = null;//displayed in editor for debug
		[SerializeField] private CardActor m_hooveredCard = null;//displayed in editor for debug

		private CardGameInput m_input;
		private Camera m_camera;

		//for click detection
		private CardActor m_cardOnBoardWhereLeftMouseDown = null;
		private Vector2 m_leftDownScreenPosition;
		private float m_leftDownTime;
		private float m_clickThresholdTime = .2f;
		private Vector3 m_positionBeforeDragging;

		private void Awake()
		{
			m_camera = Camera.main;
			m_input = new CardGameInput();
			m_input.Enable();
			m_input.Mouse.LeftClick.performed += _ctx => OnMouseLeftDown();
			m_input.Mouse.LeftClick.canceled += _ctx => OnMouseLeftUp();
			m_input.Mouse.RightClick.performed += _ctx => OnMouseRightDown();
		}

		private void OnDestroy()
		{
			m_input.Mouse.LeftClick.performed -= _ctx => OnMouseLeftDown();
			m_input.Mouse.LeftClick.canceled -= _ctx => OnMouseLeftUp();
			m_input.Mouse.RightClick.performed -= _ctx => OnMouseRightDown();
			m_input.Disable();
		}

		private void OnMouseLeftDown()
		{
			Vector2 mouseScreenPosition = m_input.Mouse.MousePosition.ReadValue<Vector2>();

			//if clicked outside card info ui, close it
			if (UIWindowsManager.Instance.CardInfoUI.IsShown() &&
				UIWindowsManager.Instance.CardInfoUI.IsScreenPosInsideMyWindow(mouseScreenPosition) == false)
				UIWindowsManager.Instance.CardInfoUI.HideMe();

			if (m_draggedCard != null)
			{
				Debug.Log("strange, on left click down card is still being dragged", this.gameObject);
				return;//player is dragging card already
			}

			//raycast for card
			Ray mouseRay = m_camera.ScreenPointToRay(mouseScreenPosition);
			RaycastHit hit;
			if (Physics.Raycast(mouseRay, out hit, 50.0f, m_cardLayer))//50.0f to make sure we hit something (camera is 10.0 over z=0)
			{
				CardActor hitCard = hit.transform.GetComponentInParent<CardActor>();
				if (hitCard == null)
				{
					Debug.Log("hit something else than card in cardLayer: " + hit.transform.root, this.gameObject);
					return;
				}

				//set vars for detecting clicking a card
				m_cardOnBoardWhereLeftMouseDown = hitCard;
				m_leftDownScreenPosition = mouseScreenPosition;
				m_leftDownTime = Time.time;

				switch(TurnManager.Instance.CurrentPhase)
				{
					case ETurnPhase.PlayerPlaysCard:
						if (hitCard.Owner != EOwner.Player)
							return;//trying to pick ai's card
						if (m_playerBoard.IsCardOnBoard(hitCard) == true)
							return;//trying to pick card which is already on board						
						if (hitCard.IsInGraveyard == true)
							return;//trying to pick card from graveyard

						//start dragging card
						m_draggedCard = hitCard;
						m_positionBeforeDragging = m_draggedCard.MyPosition;
						PlayerManager.Instance.OnStartDraggingCard(m_draggedCard);

						break;
					case ETurnPhase.PlayerDoesCardEffect:
						break;//vars for detecting clicking a card already set, do nothing
					case ETurnPhase.AIPlaysCard:
					case ETurnPhase.AIDoesCardEffect:
					case ETurnPhase.InitializeManagerLoadsCards:
						break;//ai's turn. block dragging card and do nothing
					default:
						Debug.Log("strange, " + TurnManager.Instance.CurrentPhase);
						break;
				}
			}
			else
			{ } //didn't click any card, do nothing
		}

		private void OnMouseLeftUp()
		{
			if (TurnManager.Instance.CurrentPhase != ETurnPhase.PlayerPlaysCard &&
				TurnManager.Instance.CurrentPhase != ETurnPhase.PlayerDoesCardEffect)
				return;//it's AI turn

			//clear "no clickable card" warning
			UIWindowsManager.Instance.EffectUI.ClearWarningWhenCoClickableCardOnBoard();

			Vector2 mouseScreenPosition = m_input.Mouse.MousePosition.ReadValue<Vector2>();
			Ray mouseRay = m_camera.ScreenPointToRay(mouseScreenPosition);
			RaycastHit hit;

			switch(TurnManager.Instance.CurrentPhase)
			{
				case ETurnPhase.PlayerPlaysCard:
					//when dragging card, raycast slot under mouse
					if (m_draggedCard != null)
					{
						//if left mouse up on card slot
						if (Physics.Raycast(mouseRay, out hit, 50.0f, m_slotLayer))
						{
							var hitCardSlot = hit.transform.GetComponentInParent<CardSlotActor>();
							if (hitCardSlot == null)
							{
								Debug.Log("strange, hit something with drag slot tag " + hit.transform.name, this.gameObject);
								return;
							}
							if (m_playerBoard.IsSlotRowFull(hitCardSlot))//row of slots full
							{
								m_draggedCard.MoveMeTo(m_positionBeforeDragging);
								m_draggedCard = null;
								m_playerBoard.HideSlotColliders();
								return;
							}
							if (hitCardSlot.MyCard == m_draggedCard) //corner case: card was dragged into this slot
																		//then away then is dragged into this slot again
							{
								PlayerManager.Instance.OnDropCard(m_draggedCard, hitCardSlot);
								m_draggedCard = null;
								return;
							}
							if (hitCardSlot.MyCard != null)//card slot not empty -> return card to initial position
							{
								m_draggedCard.MoveMeTo(m_positionBeforeDragging);
								m_draggedCard = null;
								m_playerBoard.HideSlotColliders();
								return;
							}
							PlayerManager.Instance.OnDropCard(m_draggedCard, hitCardSlot);
							m_draggedCard = null;
						}
						//left mouse up on plane -> return card to its initial position
						else if (Physics.Raycast(mouseRay, out hit, 50.0f, m_dragPlaneLayer))
						{
							m_draggedCard.MoveMeTo(m_positionBeforeDragging);
							m_draggedCard = null;
							m_playerBoard.HideSlotColliders();
						}
						else
							Debug.Log("Strange, didn't hit plane. dragDropPlane is so big that we should always hit it", this.gameObject);
					}
					else
					{ }//we're not dragging card

					//if left up on card -> show card info UI
					if (Physics.Raycast(mouseRay, out hit, 50.0f, m_cardLayer))
					{
						CardActor hitCard = hit.transform.GetComponentInParent<CardActor>();
						if (hitCard != null &&
							hitCard == m_cardOnBoardWhereLeftMouseDown &&
							hitCard.IsInGraveyard == false &&
							hitCard.isCardFaceUp == true &&//don't reveal properties of card face down
							IfPlacesSimilar(mouseScreenPosition, m_leftDownScreenPosition) &&
							Time.time - m_leftDownTime < m_clickThresholdTime)
						{
							//show ui about card
							UIWindowsManager.Instance.CardInfoUI.ShowMe(hitCard);
						}
					}
					break;
				case ETurnPhase.PlayerDoesCardEffect:
					//detect clicking card -> apply effect
					if (Physics.Raycast(mouseRay, out hit, 50.0f, m_cardLayer))
					{
						CardActor hitCard = hit.transform.GetComponentInParent<CardActor>();
						if (hitCard == null)
						{
							Debug.Log("strange, hit something on card layer but not card: " + hit.transform.name, this.gameObject);
							return;
						}
						if(hitCard == m_cardOnBoardWhereLeftMouseDown &&
							IfPlacesSimilar(mouseScreenPosition, m_leftDownScreenPosition) && 
							Time.time - m_leftDownTime < m_clickThresholdTime &&
							hitCard.IsInGraveyard == false)
						{
							EffectManager.Instance.OnPlayerDoingEffectClickedCard(hitCard);
						}
					}
					break;
			}
		}

		private bool IfPlacesSimilar(Vector2 placeA, Vector2 placeB)
		{
			float threshold = .01f;//.01f determined by playing
			return Mathf.Abs(placeA.x - placeB.x) < threshold && Mathf.Abs(placeA.y - placeB.y) < threshold;
		}

		/// <summary>
		/// handle dragging
		/// </summary>
		private void Update()
		{			   
			//check for drag plane
			Vector2 mouseScreenPosition = m_input.Mouse.MousePosition.ReadValue<Vector2>();
			Ray mouseRay = m_camera.ScreenPointToRay(mouseScreenPosition);
			RaycastHit hit;

			//if hit card > change cursor
			if (m_draggedCard == null)//if player is not dragging card
			{
				if (Physics.Raycast(mouseRay, out hit, 50.0f, m_cardLayer))
				{
					CardActor hitCard = hit.transform.GetComponentInParent<CardActor>();
					if (hitCard == null)
						Debug.Log("strange, hit something on card layer but not card: " + hit.transform.name, this.gameObject);
					else
					{
						//cursor over card
						CursorManager.Instance.UpdateCursorOverCard(hitCard);
						if (m_hooveredCard != null)
						{
							m_hooveredCard.OnMouseStopsHoover();
							m_hooveredCard = null;
						}
						m_hooveredCard = hitCard;
						m_hooveredCard.OnMouseHoover();
					}
				}
				else
				{
					//didn't hit card
					CursorManager.Instance.ClearCursorToDefault();
					if (m_hooveredCard != null)
					{
						m_hooveredCard.OnMouseStopsHoover();
						m_hooveredCard = null;
					}
				}
			}
			else
			{
				//player is dragging card
				CursorManager.Instance.ClearCursorToDefault();
				if (m_hooveredCard != null)
				{
					m_hooveredCard.OnMouseStopsHoover();
					m_hooveredCard = null;
				}
			}

			//if hit card slot
			if (Physics.Raycast(mouseRay, out hit, 50.0f, m_slotLayer))
			{
				if (m_draggedCard == null)
					return;//player is not dragging card
				var hitCardSlot = hit.transform.GetComponentInParent<CardSlotActor>();
				if (hitCardSlot == null)
				{
					Debug.Log("strange, hit something with drag slot tag " + hit.transform.name, this.gameObject);
					return;
				}
				if (m_playerBoard.IsSlotRowFull(hitCardSlot))
				{
					//row of slots full, slide card a little over card slot
					m_draggedCard.MoveMeTo_Instant(hit.point + new Vector3(0f, 0f, -.05f));
					return;
				}
				PlayerManager.Instance.OnCardDraggedOverSlot(m_draggedCard, hitCardSlot);
			}
			//if hit drag plane
			else if (Physics.Raycast(mouseRay, out hit, 50.0f, m_dragPlaneLayer))
			{
				if (m_draggedCard == null)
					return;//player is not dragging card
				//slide card on drag plane
				m_draggedCard.MoveMeTo_Instant(hit.point);
			}
			else 
				Debug.Log("Strange, didn't hit plane. dragDropPlane is so big that we should always hit it", this.gameObject);		
		}

		private void OnMouseRightDown()
		{
			UIWindowsManager.Instance.CardInfoUI.HideMe();//right click closes card info
		}

		[HideInInspector] public readonly string CARD_SLOT_TAG = "DragPlane_Slot";
		[HideInInspector] public readonly string DRAG_PLANE_TAG = "DragPlane_Plane";
	}
}