using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

namespace CardGame
{
	/// <summary>
	/// gets mouse events from <see cref="MouseManager"/> and sends commands to <see cref="SlotBoard"/>
	/// 
	/// similar to <see cref="AIManager"/>, but they are very different, don't refactor
	/// </summary>
	public class PlayerManager : MonoBehaviour
	{
		public static PlayerManager Instance;

		[SerializeField] private SlotBoard m_playerBoard;
		[SerializeField] private SlotBoard m_aiBoard;
		[SerializeField] private Graveyard m_playerGraveyard;
		[SerializeField] private GameObject m_handGO;

		[Header("Debug: cards")]
		[SerializeField] private List<CardActor> m_cardsOnHand;

		//additional vars
		private float lastTimeCardWasDraggedOverSlot;
		private CardSlotActor lastSlotUnderCardThatCausedRepositon;

		public int CountOfHand { get => m_cardsOnHand.Count; }

		private void Awake()
		{
			Instance = this;
		}

		public void SetHand(List<CardActor> cards)
		{
			m_cardsOnHand = cards
				.OrderBy(card => card.transform.position.x)
				.ToList();
			foreach (var card in m_cardsOnHand)
				card.Owner = EOwner.Player;
		}

		public void OnStartDraggingCard(CardActor draggedCard)
		{
			m_playerBoard.ShowCollidersForDraggedCard();

			//show info that effect will be lost if there are no clickable units
			foreach(var effect in draggedCard.Data.Effects)
			{
				if(effect.NeedsPlayerToClick && 
					effect.Target == EEffectTarget.EnemyCardOnBoard &&
					m_aiBoard.AreAllSlotsEmpty())
				{
					UIWindowsManager.Instance.EffectUI.ShowWarningWhenNoClickableEnemyOnBoard();
				}
				else if(effect.NeedsPlayerToClick &&
					effect.Target == EEffectTarget.MyCardOnBoard &&
					m_playerBoard.AreAllSlotsEmpty())
				{
					UIWindowsManager.Instance.EffectUI.ShowWarningWhenNoClickableAllyOnBoard();
				}

			}

		}

		public void OnDropCard(CardActor droppedCard, CardSlotActor slot)
		{
			Assert.IsFalse(TurnManager.Instance.CurrentPhase != ETurnPhase.PlayerPlaysCard);

			//move card to slot
			m_playerBoard.AssignAndMoveCardToSlot(droppedCard, slot);

			//add card to cardList
			m_playerBoard.AddCardToCardList(droppedCard, slot);

			//remove from player's hand
			Assert.IsFalse(m_cardsOnHand.Contains(droppedCard) == false, "player's hand doesn't have dropped card");
			m_cardsOnHand.Remove(droppedCard);

			//reposition cards to be at center of slots
			m_playerBoard.RepositionAllCardsToCenterOfSlots();

			//hide slot colliders
			m_playerBoard.HideSlotColliders();

			//if card has effects requiring clicking
			TurnManager.Instance.OnPlayerPlayedCard(droppedCard);
		}

		/// <summary>
		/// inserts dragged card into slot. The card is not dropped, it will be dropped in <see cref="OnDropCard(CardActor, CardSlotActor)"/>
		/// run from Update(), optimise
		/// </summary>
		public void OnCardDraggedOverSlot(CardActor draggedCard, CardSlotActor slotUnderCard)
		{
			//optimisation: don't reposition two times in a rowfor the same slot (this function is run in Update())
			if (lastSlotUnderCardThatCausedRepositon == slotUnderCard)
			{
				//corner case: card is dragged onto slot, then dragged away, then onto slot again
				//then just position card into slot (snap to slot) again
				if (slotUnderCard.MyCard == draggedCard)
					m_playerBoard.AssignAndMoveCardToSlot(draggedCard, slotUnderCard, true);
				return;
			}
			lastSlotUnderCardThatCausedRepositon = slotUnderCard;

			m_playerBoard.InsertCardIntoSlot(draggedCard, slotUnderCard);
		}

		public void RemoveCard(CardActor card)
		{
			PlayerManager.RemoveCardFromGame(card, m_playerBoard, m_cardsOnHand,
				m_playerGraveyard);
		}

		/// <summary>
		/// static not to repeat code in <see cref="AIManager"/>
		/// </summary>
		public static void RemoveCardFromGame(CardActor card, SlotBoard board, List<CardActor> hand,
			Graveyard targetGraveyard)
		{
			//remove from board
			if(board.IsCardOnBoard(card))
			{
				board.RemoveCard(card);
				targetGraveyard.MoveCardToGraveyard(card);
				return;
			}
			//remove from hand
			if(hand.Contains(card))
			{
				hand.Remove(card);
				targetGraveyard.MoveCardToGraveyard(card);
				return;
			}
			Debug.Log("didn't find card to remove: " + card);
		}

		public List<CardActor> GiveMyHand_ForEffects()
		{
			return m_cardsOnHand;///if updated, change also <see cref="AIManager.GiveMyHand_ForEffects"/>
		}

	}

}
