using Cinemachine;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGame
{
	public class CamShakeManager : MonoBehaviour
	{
		public static CamShakeManager Instance;

		[SerializeField] private float zoomAmout = 2.0f;
		[SerializeField] private CinemachineImpulseSource m_impulseSource;
		[SerializeField] private Transform m_parentCameraTransform;

		private void Awake()
		{
			Instance = this;
		}

		//[Button("ShakeCamera()")]//Removed Odin
		public void ShakeCamera()
		{
			m_impulseSource.GenerateImpulse();
		}

		//[Button("ZoomCamera()")]//Removed Odin
		public void ZoomCamera()
		{
			Vector3 initialPosition = m_parentCameraTransform.position;
			float duration = ConfigManager.Instance.Configuration.CardRedFlashDuration;
			var s = DOTween.Sequence()
				.Append(m_parentCameraTransform.DOMove(initialPosition + new Vector3(0, 0, zoomAmout), duration / 3.0f))
				.AppendInterval(duration / 3.0f)
				.Append(m_parentCameraTransform.DOMove(initialPosition, duration / 3.0f));
		}
	}
}