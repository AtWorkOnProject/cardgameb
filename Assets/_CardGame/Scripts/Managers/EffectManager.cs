using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace CardGame
{
	public class EffectManager : MonoBehaviour
	{
		public static EffectManager Instance;

		[Header("Boards")]
		[SerializeField] private SlotBoard m_aiBoard;
		[SerializeField] private SlotBoard m_playerBoard;

		[Header("Debug")]
		/*[DisplayAsString]*/ [SerializeField] private CardActor m_currentlyActivatedCard;//serialize field for debug
		/*[DisplayAsString]*/ [SerializeField] private EffectSO m_currentlyActivatedEffect;//serialize field for debug

		private float m_lastTimeFlashedCardsALittle;

		public EHealingOrDamage CurrentHealOrDamage
		{
			get
			{
				if (m_currentlyActivatedEffect == null)
					return EHealingOrDamage.NoEffectActive;

				if (m_currentlyActivatedEffect.HealthChange > 0)
					return EHealingOrDamage.Healing;
				return EHealingOrDamage.Damage;
			}
		}

		private void Awake()
		{
			Instance = this;
		}

		private void Update()
		{
			//flash cards a little every some seconds
			if(Time.time - m_lastTimeFlashedCardsALittle >= 3.0f &&
				m_currentlyActivatedEffect != null)
			{
				if(m_currentlyActivatedEffect.Target == EEffectTarget.EnemyCardOnBoard)
					m_aiBoard.FlashAllCardsALittle();
				else if (m_currentlyActivatedEffect.Target == EEffectTarget.MyCardOnBoard)
					m_playerBoard.FlashAllCardsALittle();
				m_lastTimeFlashedCardsALittle = Time.time;
			}
		}

		/// <summary>
		/// used also by AI
		/// </summary>
		public static void ApplyEffectOnCard(EffectSO effect, CardActor affectedCard, CardActor cardThatAffects)
		{
			//health
			if (effect.ChangeHealth == true)
			{
				affectedCard.Health += effect.HealthChange;
				if(affectedCard.isCardFaceUp && affectedCard.IsInGraveyard == false)
					RisingText.CreateRisingText(affectedCard.PosOfHealthText, effect.HealthChange);
				
				if (effect.HealthChange < 0)
				{
					//dmage
					affectedCard.FlashRed();
					affectedCard.PlayHitVFX();
					CamShakeManager.Instance.ShakeCamera();
					if (cardThatAffects.Owner == EOwner.Player)
						CamShakeManager.Instance.ZoomCamera();//if we hit, also zoom camera
				}else if(effect.HealthChange > 0)
				{
					//healing
					affectedCard.FlashGreen();
				}

				//check for removed card
				if (affectedCard.Health <= 0)
				{
					EffectManager.Instance.ChangeHealthOnCardKilled(affectedCard);//trigger other cards that have "change health when enemy killed" effect

					if (affectedCard.Owner == EOwner.Player)
						PlayerManager.Instance.RemoveCard(affectedCard);
					else
						AIManager.Instance.RemoveCard(affectedCard);
				}
			}
		}

		private void ChangeHealthOnCardKilled(CardActor c)
		{
			//activate "change health when enemy killed" effects
			if (c.Owner == EOwner.AI)
				ChangeHealthToCardsOnBoard_WhenEnemyKilled(m_playerBoard);
			else
				ChangeHealthToCardsOnBoard_WhenEnemyKilled(m_aiBoard);
		}

		private void ChangeHealthToCardsOnBoard_WhenEnemyKilled(SlotBoard board)
		{
			foreach (var card in board.GetAllCardsFromBoard())
			{
				foreach (var cardEffect in card.Data.Effects)
				{
					if (cardEffect.ChangeHealthWhenEnemyKilled == true)
					{

						card.Health += cardEffect.HealthChangeWhenEnemyKilled;
						if (card.isCardFaceUp && card.IsInGraveyard == false)
							RisingText.CreateRisingText(card.PosOfHealthText, cardEffect.HealthChangeWhenEnemyKilled);

						if (cardEffect.HealthChangeWhenEnemyKilled > 0)
							//healing
							card.FlashGreen();
						else
							//damage
							card.FlashRed();
						if (card.Health <= 0)
						{
							EffectManager.Instance.ChangeHealthOnCardKilled(card);//if card is killed, trigger effects "change helath when enemy killed" again

							if (card.Owner == EOwner.Player)
								PlayerManager.Instance.RemoveCard(card);
							else
								AIManager.Instance.RemoveCard(card);
						}
					}
				}
			}
		}

		public void ChangeHealthOnTurnEnd()
		{
			TriggerHealthChangeOnTurnEnd(PlayerManager.Instance.GiveMyHand_ForEffects());
			TriggerHealthChangeOnTurnEnd(AIManager.Instance.GiveMyHand_ForEffects());
		}

		public void TriggerHealthChangeOnTurnEnd(List<CardActor> cardsOnHand)
		{
			List<CardActor> tmpCardsOnHand = new List<CardActor>(cardsOnHand);//if card on hand dies, card list gets modified
			//trigger changing health at end of turn
			foreach(var card in tmpCardsOnHand)
			{
				foreach (var cardEffect in card.Data.Effects)
				{
					if (cardEffect.ChangeHealthInHandAtEndOfTurn == true)
					{
						card.Health += cardEffect.HealthChangeInHandEndOfTurn;
						if (card.isCardFaceUp && card.IsInGraveyard == false)
							RisingText.CreateRisingText(card.PosOfHealthText, cardEffect.HealthChangeInHandEndOfTurn);

						if (cardEffect.HealthChangeInHandEndOfTurn > 0)
							//healing
							card.FlashGreen();
						else
							//damage
							card.FlashRed();
						if (card.Health <= 0)
						{
							//card from hand is not on board, don't change health of cards on board
							//EffectManager.Instance.TriggerHealthChangeWhenCardKilled(card);

							if (card.Owner == EOwner.Player)
								PlayerManager.Instance.RemoveCard(card);
							else
								AIManager.Instance.RemoveCard(card);
						}

					}
				}
			}
		}

		/// <summary>
		/// player activated card with effects
		/// </summary>
		public void OnPlayerPlayedCard(CardActor card)
		{
			m_currentlyActivatedCard = card;
			if (card.Data.Effects[0].NeedsPlayerToClick)
			{
				ActivateEffect(card.Data.Effects[0]);
			}
			else
			{
				m_currentlyActivatedEffect = card.Data.Effects[0];
				SearchActivateNextEffect();
			}
		}

		private void ActivateEffect(EffectSO effect)
		{
			m_currentlyActivatedEffect = effect;
			if (m_currentlyActivatedEffect.Target == EEffectTarget.EnemyCardOnBoard)
			{
				UIWindowsManager.Instance.EffectUI.ShowClickEnemyCard();
				//m_aiBoard.FlashAllCardsALittle();//moved to Update() todo remove comment

				if (m_aiBoard.AreAllSlotsEmpty())
					SearchActivateNextEffect();//if there are no cards on board -> treat effect as consumed
			}
			else if (m_currentlyActivatedEffect.Target == EEffectTarget.MyCardOnBoard)
			{
				UIWindowsManager.Instance.EffectUI.ShowClickPlayerCard();
				//m_playerBoard.FlashAllCardsALittle();//moved to Update() todo remove comment

				if (m_playerBoard.AreAllSlotsEmpty())
					SearchActivateNextEffect();//if there are no cards on board -> treat effect as consumed
			}
			//now wait till player clicks card
		}

		/// <summary>
		/// clicked card while processing effects
		/// </summary>
		public void OnPlayerDoingEffectClickedCard(CardActor clickedCard)
		{
			Assert.IsFalse(TurnManager.Instance.CurrentPhase != ETurnPhase.PlayerDoesCardEffect);

			//check if clicked wrong card
			if (m_currentlyActivatedEffect.Target == EEffectTarget.EnemyCardOnBoard)
			{
				if (clickedCard.Owner != EOwner.AI ||
					m_aiBoard.IsCardOnBoard(clickedCard) == false)
					return;//wrong card
			}
			else if (m_currentlyActivatedEffect.Target == EEffectTarget.MyCardOnBoard)
			{
				if (clickedCard.Owner != EOwner.Player ||
					m_playerBoard.IsCardOnBoard(clickedCard) == false)
					return;//wrong card
			}

			ApplyEffectOnCard(m_currentlyActivatedEffect, clickedCard, m_currentlyActivatedCard);

			SearchActivateNextEffect();
		}

		private void SearchActivateNextEffect()
		{
			//find next effect that needs click
			int currentEffectIndex = m_currentlyActivatedCard.Data.Effects.IndexOf(m_currentlyActivatedEffect);
			EffectSO effectThatNeedsClick = null;
			for(
				int i = ++currentEffectIndex; //start from next effect
				i < m_currentlyActivatedCard.Data.Effects.Count; 
				i++
				)
			{
				var effect = m_currentlyActivatedCard.Data.Effects[i];

				if (effect.NeedsPlayerToClick == false)
					continue;//continue if effect doesn't need player's click
				effectThatNeedsClick = effect;
				break;
			}

			//if this was the last effect
			if (effectThatNeedsClick == null)
			{
				m_currentlyActivatedCard = null;
				m_currentlyActivatedEffect = null;
				TurnManager.Instance.OnPlayerFinishedProcessingEffects();
				UIWindowsManager.Instance.EffectUI.ClearClickCard();
				return;
			}

			//activate next effect
			ActivateEffect(effectThatNeedsClick);
		}
	}

	public enum EHealingOrDamage
	{
		Healing,
		Damage,
		NoEffectActive,
	}
}