using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

namespace CardGame
{
	/// <summary>
	/// does AI turn move
	/// 
	/// counterpart for player: <see cref="PlayerManager"/>, but they are very different, don't refactor
	/// </summary>
	public class AIManager : MonoBehaviour
	{
		public static AIManager Instance;

		[SerializeField] private SlotBoard m_aiBoard;
		[SerializeField] private SlotBoard m_playerBoard;
		[SerializeField] private Graveyard m_aiGraveyard;
		[SerializeField] private GameObject m_handGO;

		[Header("Debug: cards")]
		[SerializeField] private List<CardActor> m_cardsOnHand;

		//way to deal with delaying ai playing effects after card is placed on board.
		//I don't use callbacks as they complicated arhitecture
		public CardActor CardWithEffectToDoAfterMove;

		public int CountOfHand { get => m_cardsOnHand.Count; }

		private void Awake()
		{
			Instance = this;
		}

		public void SetHand(List<CardActor> cards)
		{
			m_cardsOnHand = cards
				.OrderBy(card => card.transform.position.x)
				.ToList();
			foreach (var card in m_cardsOnHand)
			{
				card.Owner = EOwner.AI;
				card.RotateToBackUp_Instant();
			}
		}

		//[Sirenix.OdinInspector.Button("DoNextAIMove()")] //Removed Odin
		public void PlayNextCard()
		{
			if (m_cardsOnHand.Count == 0)
			{
				Debug.Log("no cards left");
				TurnManager.Instance.OnAIFinishedMove();
				return;
			}

			CardActor playedCard = m_cardsOnHand[Random.Range(0, m_cardsOnHand.Count)];

			m_aiBoard.AI_GetData(out int upperCardCount, out int upperSlotsCount,
				out int bottomCardsCount, out int bottomSlotsCount);

			if (upperCardCount < upperSlotsCount)
				m_aiBoard.AI_AddToUpperCardList(playedCard);
			else if (bottomCardsCount < bottomSlotsCount)
				m_aiBoard.AI_AddToBottomCardList(playedCard);
			else
			{
				Debug.Log("no free slots");
				TurnManager.Instance.OnAIFinishedMove();
				return;
			}

			m_cardsOnHand.Remove(playedCard);

			//process effects, after card is moved on board
			if (playedCard.HasEffectsRequiringPlayerToClick)
			{
				CardWithEffectToDoAfterMove = playedCard;
				//reposition will trigger action when card is placed on board
				m_aiBoard.RepositionAllCardsToCenterOfSlots();
			}
			else
			{
				CardWithEffectToDoAfterMove = null;
				m_aiBoard.RepositionAllCardsToCenterOfSlots();
				TurnManager.Instance.OnAIFinishedMove();
			}
		}

		/// <summary>
		/// called many times (for each ai's card on the board), 
		/// so use m_cardWhichHasEffectsToProcess to consume effects
		/// </summary>
		public void ProcessCardsEffectsAfterCardMovedOnBoard()
		{
			Assert.IsFalse(CardWithEffectToDoAfterMove == null);

			foreach (var effect in CardWithEffectToDoAfterMove.Data.Effects)
			{
				if(effect.NeedsPlayerToClick)//process only effects that need clicking
					ProcessCardEffect(effect, CardWithEffectToDoAfterMove);
			}

			CardWithEffectToDoAfterMove = null;
		}

		private void ProcessCardEffect(EffectSO effect, CardActor cardThatAffects)
		{
			CardActor affectedCard = null;
			if (effect.Target == EEffectTarget.EnemyCardOnBoard)
			{
				if (m_playerBoard.AreAllSlotsEmpty())
					return;//no cards to apply card on
				affectedCard = m_playerBoard.AI_GetRandomCard();//AI's enemy is player
			}
			else
			{
				affectedCard = m_aiBoard.AI_GetRandomCard();
			}
			EffectManager.ApplyEffectOnCard(effect, affectedCard, cardThatAffects);
		}

		public void RemoveCard(CardActor card)
		{
			//use one code for both player and ai
			PlayerManager.RemoveCardFromGame(card, m_aiBoard, m_cardsOnHand, 
				m_aiGraveyard);
		}

		public List<CardActor> GiveMyHand_ForEffects()
		{
			return m_cardsOnHand;///if updated, change also <see cref="PlayerManager.GiveMyHand_ForEffects"/>
		}
	}
}
