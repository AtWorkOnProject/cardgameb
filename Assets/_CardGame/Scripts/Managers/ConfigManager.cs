using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGame
{
	public class ConfigManager : MonoBehaviour
	{
		public static ConfigManager Instance;

		public ConfigurationSO Configuration;

		private void Awake()
		{
			Instance = this;
		}

	}
}
