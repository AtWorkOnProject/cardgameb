using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGame
{
	public class CursorManager : MonoBehaviour
	{
		public static CursorManager Instance;

		public Texture2D DefaultCursor;
		public Texture2D SelectCursor;
		public Texture2D HealCursor;
		public Texture2D AttackCursor;

		[Header("Boards")]
		[SerializeField] private SlotBoard m_aiBoard;
		[SerializeField] private SlotBoard m_playerBoard;

		ECursor currentCursor;

		private void Awake()
		{
			Instance = this;
		}

		private void Start()
		{
			currentCursor = ECursor.Attack;//something not default, SetCursor won't set cursor if cursor is already set to that type
			ClearCursorToDefault();
		}

		public void UpdateCursorOverCard(CardActor card)
		{
			switch(EffectManager.Instance.CurrentHealOrDamage)
			{
				case EHealingOrDamage.NoEffectActive:
					if (card.Owner == EOwner.Player)
						SetCursor(ECursor.Select);
					else
						ClearCursorToDefault();
					break;
				case EHealingOrDamage.Healing:
					if (card.Owner == EOwner.Player &&
						m_playerBoard.IsCardOnBoard(card))
						SetCursor(ECursor.Heal);
					else
						ClearCursorToDefault();
					break;
				case EHealingOrDamage.Damage:
					if (card.Owner == EOwner.AI &&
						m_aiBoard.IsCardOnBoard(card))
						SetCursor(ECursor.Attack);
					else
						ClearCursorToDefault();
					break;
			}
		}

		public void ClearCursorToDefault()
		{
			SetCursor(ECursor.Default);
		}

		private void SetCursor(ECursor cursorType)
		{
			if (currentCursor == cursorType)
				return;//already set
			switch(cursorType)
			{
				case ECursor.Default:
					Cursor.SetCursor(DefaultCursor,
						new Vector2(1, 1),
						CursorMode.Auto);
					break;
				case ECursor.Attack:
					Cursor.SetCursor(AttackCursor,
						new Vector2(1, 1),
						CursorMode.Auto);
					break;
				case ECursor.Heal:
					Cursor.SetCursor(HealCursor,
						new Vector2(1, 1),
						CursorMode.Auto);
					break;
				case ECursor.Select:
					Cursor.SetCursor(SelectCursor,
						new Vector2(1, 1),
						CursorMode.Auto);
					break;
			}
			currentCursor = cursorType;
		}
	}

	public enum ECursor
	{
		Default,
		Attack,
		Heal,
		Select,
	}
}