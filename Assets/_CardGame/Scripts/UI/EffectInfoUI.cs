using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGame
{
	/// <summary>
	/// managed by <see cref="EffectManager"/>
	/// </summary>
	public class EffectInfoUI : MonoBehaviour
	{
		[SerializeField] private TMPro.TextMeshProUGUI EffectText;
		[SerializeField] private TMPro.TextMeshProUGUI WarningText;

		private void Start()
		{
			//hide texts at start
			ClearClickCard();
			ClearWarningWhenCoClickableCardOnBoard();
		}

		public void ClearClickCard()
		{
			EffectText.text = "";
		}

		public void ShowClickEnemyCard()
		{
			EffectText.text = "Click enemy card to activate effect!";
		}

		public void ShowClickPlayerCard()
		{
			EffectText.text = "Click your card to activate effect!";
		}

		public void ShowWarningWhenNoClickableEnemyOnBoard()
		{
			WarningText.text = "No enemy card to use effect on, effect of card will be lost!";
		}

		public void ShowWarningWhenNoClickableAllyOnBoard()
		{
			WarningText.text = "No ally card to use effect on, effect of card will be lost!";
		}

		public void ClearWarningWhenCoClickableCardOnBoard()
		{
			WarningText.text = "";
		}
	}
}