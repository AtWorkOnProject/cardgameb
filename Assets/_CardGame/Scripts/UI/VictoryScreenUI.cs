using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGame
{
	public class VictoryScreenUI : MonoBehaviour
	{
		[SerializeField] private TMPro.TextMeshProUGUI m_victoryText;
		[SerializeField] private TMPro.TextMeshProUGUI m_defeatText;
		[SerializeField] private TMPro.TextMeshProUGUI m_drawText;

		public void ShowVictoryScreen(VictoryOrDefeat whatToShow)
		{
			//hide all texts
			m_victoryText.gameObject.SetActive(false);
			m_defeatText.gameObject.SetActive(false);
			m_drawText.gameObject.SetActive(false);

			switch(whatToShow)
			{
				case VictoryOrDefeat.Victory:
					m_victoryText.gameObject.SetActive(true);
					break;
				case VictoryOrDefeat.Defeat:
					m_defeatText.gameObject.SetActive(true);
					break;
				case VictoryOrDefeat.Draw:
					m_drawText.gameObject.SetActive(true);
					break;
			}
			this.gameObject.SetActive(true);
		}

		public void OnQuitButtonClicked()
		{
			MenuWindowUI.QuitCardGame();
		}
	}

	public enum VictoryOrDefeat
	{
		Defeat,
		Victory,
		Draw,
	}
}