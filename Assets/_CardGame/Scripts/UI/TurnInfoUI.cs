using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGame
{
	public class TurnInfoUI : MonoBehaviour
	{
		[SerializeField] private TMPro.TextMeshProUGUI m_turnText;
		[SerializeField] private TMPro.TextMeshProUGUI m_newTurnText;
		[SerializeField] private float m_newTurnTextSpeed = 5.0f;

		private float m_targetX;

		private void Update()
		{
			//animate new turn text
			if(m_newTurnText.gameObject.activeSelf)
			{
				m_newTurnText.transform.localPosition = new Vector3(
					Mathf.Lerp(m_newTurnText.transform.localPosition.x, m_targetX, Time.deltaTime * m_newTurnTextSpeed),
					m_newTurnText.transform.localPosition.y,
					m_newTurnText.transform.localPosition.z
					);
				if (Mathf.Abs(m_newTurnText.transform.localPosition.x - m_targetX) < .1f)
					m_newTurnText.gameObject.SetActive(false);
			}
		}

		public void SetTurn(int turn)
		{
			//set new turn text animation
			m_newTurnText.text = "turn " + turn;
			m_targetX = 300.0f;
			m_newTurnText.transform.localPosition = new Vector3(
				0,
				m_newTurnText.transform.localPosition.y,
				m_newTurnText.transform.localPosition.z
				);
			m_newTurnText.gameObject.SetActive(true);

			//set turn text
			m_turnText.text = "turn " + turn;
		}


	}
}