using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CardGame
{
	public class CardInfoUI : MonoBehaviour
	{
		[SerializeField] private TMPro.TextMeshProUGUI HeaderText;
		[SerializeField] private TMPro.TextMeshProUGUI HealthText;
		[SerializeField] private TMPro.TextMeshProUGUI DescriptionText;
		[SerializeField] private Image ImageWithSprite;

		private RectTransform m_rectTransform;

		private void Awake()
		{
			m_rectTransform = this.GetComponent<RectTransform>();
		}
		public void OnBackButtonClicked()
		{
			HideMe();
		}

		public void HideMe()
		{
			this.gameObject.SetActive(false);
		}

		public void ShowMe(CardActor card)
		{
			HeaderText.text = card.Data.Header;
			HealthText.text = card.Health.ToString();
			DescriptionText.text = card.Data.Description;
			ImageWithSprite.sprite = card.Data.Sprite;
			ImageWithSprite.color = card.Data.Tint;
			this.gameObject.SetActive(true);
		}

		public bool IsShown()
		{
			return this.gameObject.activeSelf;
		}

		public bool IsScreenPosInsideMyWindow(Vector2 screenPosition)
		{
			return RectTransformUtility.RectangleContainsScreenPoint(m_rectTransform, screenPosition);
		}
	}
}