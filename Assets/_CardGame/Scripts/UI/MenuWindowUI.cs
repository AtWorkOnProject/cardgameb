using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuWindowUI : MonoBehaviour
{
    
	public void OnResumeClicked()
	{
		ToggleMe();
	}

	public void OnQuitClicked()
	{
		QuitCardGame();
	}

	public void ToggleMe()
	{
		this.gameObject.SetActive(!this.gameObject.activeSelf);
	}

	public static void QuitCardGame()
	{
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
			Application.Quit();
#endif
	}
}
