using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGame
{
	/// <summary>
	/// access point for ui windows
	/// </summary>
	public class UIWindowsManager : MonoBehaviour
	{
		public static UIWindowsManager Instance;

		[Header("Child windows")]
		public CardInfoUI CardInfoUI;
		public TurnInfoUI TurnUI;
		public PointsInfoUI PointsUI;
		public VictoryScreenUI VictoryUI;
		public EffectInfoUI EffectUI;
		public MenuWindowUI MenuUI;

		private void Awake()
		{
			Instance = this;
		}

		private void Update()
		{
			//escape button pressed
			if (UnityEngine.InputSystem.Keyboard.current.escapeKey.wasPressedThisFrame)
				MenuUI.ToggleMe();
		}
	}
}
