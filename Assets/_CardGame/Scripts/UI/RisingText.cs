using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGame
{
	public class RisingText : MonoBehaviour
	{
		public static RisingText PrefabOfFirstRisingText = null;

		[SerializeField] private TMPro.TextMeshProUGUI m_text;
		[Space]
		/*[DisplayAsString]*/ [SerializeField] private float m_timeCreated;
		/*[DisplayAsString]*/ [SerializeField] private Vector3 m_startWorldPosition;

		private void Awake()
		{
			if (PrefabOfFirstRisingText == null)//first rising text is in scene
			{
				PrefabOfFirstRisingText = this;
				this.gameObject.SetActive(false);
			}
		}

		public static void CreateRisingText(Vector3 worldPosition, int healthChange)
		{
			RisingText newRisingText = Instantiate(PrefabOfFirstRisingText, PrefabOfFirstRisingText.transform.parent);
			if (healthChange >= 0)
			{
				newRisingText.m_text.SetText("+" + healthChange);
				newRisingText.name = "+" + healthChange.ToString();
				newRisingText.m_text.color = Color.green;
			}
			else
			{
				newRisingText.m_text.SetText(healthChange.ToString());
				newRisingText.name = healthChange.ToString();
				newRisingText.m_text.color = Color.red;
			}
			newRisingText.m_startWorldPosition = worldPosition;
			newRisingText.m_timeCreated = Time.time;//must be here not in start as first updateMyPosition() is here, here in this function is before Start
			newRisingText.updateMyPosition();
			newRisingText.transform.SetParent(PrefabOfFirstRisingText.transform.parent);
			newRisingText.gameObject.SetActive(true);
		}

		private void Update()
		{
			if (Time.time > m_timeCreated + ConfigManager.Instance.Configuration.TimeToLiveRisingText)
			{
				Destroy(this.gameObject);
				return;
			}
			updateMyPosition();
		}

		private void updateMyPosition()
		{
			Vector2 myStartPositionOnScreen = Camera.main.WorldToScreenPoint(m_startWorldPosition);//todo cache camera
			float timePassedSinceStart = Time.time - m_timeCreated;
			this.transform.position = myStartPositionOnScreen + new Vector2(
				0,
				ConfigManager.Instance.Configuration.SpeedRisingText * timePassedSinceStart
				);
		}

		//[Button("CreateMe")]//Removed Odin
		private void CraeteMeInEditor()
		{
			RisingText.CreateRisingText(new Vector3(0,0,0), 1);
		}
	}
}