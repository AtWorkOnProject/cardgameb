using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace CardGame
{
	public class PointsInfoUI : MonoBehaviour
	{
		[SerializeField] private TMPro.TextMeshProUGUI m_playerPointsText;
		[SerializeField] private TMPro.TextMeshProUGUI m_aiPointsText;

		public void SetPlayerPoints(int points)
		{
			m_playerPointsText.text = "Player: " + points.ToString();
		}

		public void SetAIPoints(int points)
		{
			m_aiPointsText.text = "Enemy: " + points.ToString();
		}
	}
}