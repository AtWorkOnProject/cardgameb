using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGame
{
	public class Graveyard : MonoBehaviour
	{
		[SerializeField] private GameObject m_visualisationPlane;

		public void MoveCardToGraveyard(CardActor card)
		{
			card.MoveTo_WithRotationToBack(this.transform.position + //move to graveyard position
				new Vector3(0,0,-.01f));//to avoid z fighting
			card.IsInGraveyard = true;
			card.HideHealthText();
		}
	}
}