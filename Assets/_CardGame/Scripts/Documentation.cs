using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGame
{
	/// <summary>
	/// For putting info on game objects
	/// </summary>
	public class Documentation : MonoBehaviour
	{
		[TextArea(3, 20)]
		[SerializeField] private string info;
	}
}
