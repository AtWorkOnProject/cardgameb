using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace CardGame
{
	/// <summary>
	/// data holder for a card, also for card's sprite
	/// 
	/// logic is in <see cref="PlayerManager"/>
	/// </summary>
	[Serializable]
	[SelectionBase]
	public class CardActor : MonoBehaviour
	{
		[Header("Card Data")]
		public CardDataSO Data;

		[Header("Visual")]
		[SerializeField] private TMPro.TextMeshPro m_healthText;
		[SerializeField] private GameObject m_healthGO;
		[SerializeField] private Transform m_healthTextPosition;
		[SerializeField] private SpriteRenderer m_sprite;
		[SerializeField] private Renderer m_frontPlane;
		[SerializeField] private ParticleSystem m_hitVFX;

		[Header("Debug")]
		/*[DisplayAsString]*/ public EOwner Owner = EOwner.Player;
		/*[DisplayAsString]*/ public bool IsInGraveyard = false;
		[HideInInspector] public Vector3 MyPosition { get => this.transform.position; }
		[HideInInspector] public Vector3 PosOfHealthText { get => m_healthTextPosition.position; }

		private Sequence m_currentMoveSequence = null;
		private Sequence m_currentMouseHooverSequence = null;
		private Sequence m_currentMouseNotHooverSequence = null;
		private Color m_initialSpriteTint;
		private Color m_initialFrontPlaneTint;
		public bool isCardFaceUp = true;

		private int m_health;
		public int Health
		{
			get => m_health;
			set
			{
				m_health = value;
				m_healthText.text = value.ToString();
			}
		}

		public bool HasEffectsRequiringPlayerToClick
		{
			get {
				foreach (var effect in Data.Effects)
					if (effect.NeedsPlayerToClick == true)
						return true;
				return false;

			}
		}

		private void Start()
		{
			SetImageFromData();
		}

		public void SetImageFromData()
		{
			this.name = Data.Header;
			Health = Data.InitialHealth;
			if (Data.Sprite != null)
				m_sprite.sprite = Data.Sprite;
			m_sprite.color = Data.Tint;
			m_sprite.transform.localScale = new Vector3(Data.SpriteScale, Data.SpriteScale, 1.0f);

			m_initialSpriteTint = m_sprite.color;
			if(Application.isPlaying)//causes leak in editor mode
				m_initialFrontPlaneTint = m_frontPlane.material.GetColor("_BaseColor");
		}

		public void MoveMeTo(Vector3 destination)
		{
			if (m_currentMoveSequence != null)
				m_currentMoveSequence.Kill();

			float duration = ConfigManager.Instance.Configuration.CardMoveDuration;
			m_currentMoveSequence = DOTween.Sequence()
				.Append(transform.DOMove(destination, duration))
				.OnComplete(() =>
				{
					TurnManager.Instance.OnCardPlacedOnBoard(this);//don't do callback, it complicates architecture
				});
		}

		public void HideHealthText()
		{
			m_healthGO.SetActive(false);
		}

		public void MoveMeTo_Instant(Vector3 destination)
		{
			if (m_currentMoveSequence != null)
				m_currentMoveSequence.Kill();

			this.transform.position = destination;
		}

		public void RotateToBackUp_Instant()
		{
			this.transform.localRotation = Quaternion.Euler(0, 180, 0);
			isCardFaceUp = false;
		}

		/// <summary>
		/// when ai moves card onto board
		/// </summary>
		public void MoveTo_WithRotationToFront(Vector3 destination)
		{
			float duration = ConfigManager.Instance.Configuration.CardMoveDuration;
			var rotateSequence = DOTween.Sequence()
				.Append(transform.DOMove(destination, duration))
				//join to move position and rotation at the same time
				.Join(transform.DORotate(new Vector3(0, 0, 0), duration))
				;
			isCardFaceUp = true;
		}

		/// <summary>
		/// when card goes to graveyard
		/// </summary>
		public void MoveTo_WithRotationToBack(Vector3 destination)
		{
			float duration = ConfigManager.Instance.Configuration.CardMoveDuration;
			var rotateSequence = DOTween.Sequence()
				.Append(transform.DOMove(destination, duration))
				.Join(transform.DORotate(new Vector3(0, 180, 0), duration))
				;

			isCardFaceUp = false;
		}

		public void FlashRed()
		{
			m_sprite.color = Color.red;
			m_frontPlane.material.color = new Color(.6f, 0f, 0f);//dark red

			float duration = ConfigManager.Instance.Configuration.CardRedFlashDuration;
			var s = DOTween.Sequence()
				.Append(m_sprite.DOColor(m_initialSpriteTint, duration))
				.Join(m_frontPlane.material.DOColor(m_initialFrontPlaneTint, "_BaseColor", duration))
				;
		}

		public void FlashGreen()
		{
			m_sprite.color = Color.green;
			m_frontPlane.material.color = new Color(0f, .6f, 0f);//dark green

			float duration = ConfigManager.Instance.Configuration.CardRedFlashDuration;
			var s = DOTween.Sequence()
				.Append(m_sprite.DOColor(m_initialSpriteTint, duration))
				.Join(m_frontPlane.material.DOColor(m_initialFrontPlaneTint, "_BaseColor", duration))
				;
		}

		/// <summary>
		/// flash a little when new effect says "click enemy card" and all enemy cards flash
		/// </summary>
		public void FlashALittle()
		{
			Color flashColor = new Color(.5f, .5f, 0f);//yellowish
			m_frontPlane.material.color = flashColor;

			float duration = ConfigManager.Instance.Configuration.CardLittleFlashDuration;
			var s = DOTween.Sequence()
				.Append(m_sprite.DOColor(m_initialSpriteTint, duration))
				.Join(m_frontPlane.material.DOColor(m_initialFrontPlaneTint, "_BaseColor", duration))
				;
		}

		public void OnMouseHoover()
		{
			if (m_currentMouseHooverSequence != null)
				return;//already during mouse hoover
			if(m_currentMouseNotHooverSequence != null)
			{
				m_currentMouseNotHooverSequence.Kill();
				m_currentMouseNotHooverSequence = null;
			}
			Color hooverColor = new Color(.6f, .6f, 0f);//yellowish
			float duration = ConfigManager.Instance.Configuration.CardSelectTintDuration;
			m_currentMouseHooverSequence = DOTween.Sequence()
				.Append(m_frontPlane.material.DOColor(hooverColor, "_BaseColor", duration))
				.OnComplete(() =>
				{
					m_currentMouseHooverSequence = null;
				})
				;
		}

		public void PlayHitVFX()
		{
			m_hitVFX.Stop();
			m_hitVFX.Play();
		}

		public void OnMouseStopsHoover()
		{
			if (m_currentMouseNotHooverSequence != null)
				return;//already during mouse not-hoover
			if (m_currentMouseHooverSequence != null)
			{
				m_currentMouseHooverSequence.Kill();
				m_currentMouseHooverSequence = null;
			}
			Color hooverColor = Color.yellow;
			float duration = ConfigManager.Instance.Configuration.CardSelectTintDuration;
			m_currentMouseNotHooverSequence = DOTween.Sequence()
				.Append(m_frontPlane.material.DOColor(m_initialFrontPlaneTint, "_BaseColor", duration))
				.OnComplete(() =>
				{
					m_currentMouseNotHooverSequence = null;
				})
				;
		}
	}

	public enum EOwner
	{
		Player = 0,
		AI = 1,
	}
}