using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGame
{
	public class SlotColliderGizmo : MonoBehaviour
	{
		[SerializeField] private Color m_gizmoColor = Color.green;
		[SerializeField] private Vector3 m_gizmoOffset;

#if UNITY_EDITOR
		private void OnDrawGizmos()
		{
			Gizmos.color = m_gizmoColor;
			Gizmos.DrawWireCube(
				transform.position + m_gizmoOffset, new Vector3(this.transform.lossyScale.x, this.transform.lossyScale.y, .01f)//thin collider
				);
		}
#endif
	}
}