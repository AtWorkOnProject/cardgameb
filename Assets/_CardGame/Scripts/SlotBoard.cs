using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

namespace CardGame
{
	/// <summary>
	/// manages placing cards in slots
	/// </summary>
	public class SlotBoard : MonoBehaviour
	{
		[Header("Parent GOs of card slots")]
		[SerializeField] private GameObject m_upperSlotsGO_Even;
		[SerializeField] private GameObject m_upperSlotsGO_Odd;
		[SerializeField] private GameObject m_bottomSlotsGO_Even;
		[SerializeField] private GameObject m_bottomSlotsGO_Odd;

		//lists of slots ordered from leftmost to rightmost
		[Header("Debug: slots")]
		[SerializeField] private List<CardSlotActor> m_upperSlots_Even;
		[SerializeField] private List<CardSlotActor> m_upperSlots_Odd;
		[SerializeField] private List<CardSlotActor> m_bottomSlots_Even;
		[SerializeField] private List<CardSlotActor> m_bottomSlots_Odd;

		//lists of cards on board. Ordered from leftmost to rightmost
		[Header("Debug: cards")]
		[SerializeField] private List<CardActor> m_upperCards;
		[SerializeField] private List<CardActor> m_bottomCards;

		private void Start()
		{
			//populate slot lists
			m_upperSlots_Even = m_upperSlotsGO_Even.GetComponentsInChildren<CardSlotActor>()
				.OrderBy(slot => slot.transform.position.x)//order list from leftmost to rightmost slot
				.ToList();
			m_upperSlots_Odd = m_upperSlotsGO_Odd.GetComponentsInChildren<CardSlotActor>()
				.OrderBy(slot => slot.transform.position.x)
				.ToList();
			m_bottomSlots_Even = m_bottomSlotsGO_Even.GetComponentsInChildren<CardSlotActor>()
				.OrderBy(slot => slot.transform.position.x)
				.ToList();
			m_bottomSlots_Odd = m_bottomSlotsGO_Odd.GetComponentsInChildren<CardSlotActor>()
				.OrderBy(slot => slot.transform.position.x)
				.ToList();

			m_upperCards = new List<CardActor>();
			m_bottomCards = new List<CardActor>();

			//disable all colliders so that player can't drag cards into ai's board
			HideSlotColliders();
		}

		public void HideSlotColliders()
		{
			foreach (var slot in m_upperSlots_Even)
				slot.SetColliderEnabled(false);
			foreach (var slot in m_upperSlots_Odd)
				slot.SetColliderEnabled(false);
			foreach (var slot in m_bottomSlots_Even)
				slot.SetColliderEnabled(false);
			foreach (var slot in m_bottomSlots_Odd)
				slot.SetColliderEnabled(false);
		}

		public void ShowCollidersForDraggedCard()
		{
			//enable slot colliders for desired number of cards (+1 is for dragged card)
			//upper slots
			if (IsEven(m_upperCards.Count + 1))
			{
				//enable even slot colliders
				foreach (var slot in m_upperSlots_Even)
					slot.SetColliderEnabled(true);
				foreach (var slot in m_upperSlots_Odd)
					slot.SetColliderEnabled(false);
			}else
			{
				//enable odd slot colliders
				foreach (var slot in m_upperSlots_Even)
					slot.SetColliderEnabled(false);
				foreach (var slot in m_upperSlots_Odd)
					slot.SetColliderEnabled(true);

			}

			//bottom slots
			if (IsEven(m_bottomCards.Count + 1))
			{ 
				foreach (var slot in m_bottomSlots_Even)
					slot.SetColliderEnabled(true);
				foreach (var slot in m_bottomSlots_Odd)
					slot.SetColliderEnabled(false);
			}
			else
			{
				foreach (var slot in m_bottomSlots_Even)
					slot.SetColliderEnabled(false);
				foreach (var slot in m_bottomSlots_Odd)
					slot.SetColliderEnabled(true);
			}

			//if slot colliders from a row are full, disable them
			if(m_upperCards.Count >= m_upperSlots_Even.Count)
				foreach (var slot in m_upperSlots_Even)
					slot.SetColliderEnabled(false);
			if (m_upperCards.Count >= m_upperSlots_Odd.Count)
				foreach (var slot in m_upperSlots_Odd)
					slot.SetColliderEnabled(false);
			if (m_bottomCards.Count >= m_bottomSlots_Even.Count)
				foreach (var slot in m_bottomSlots_Even)
					slot.SetColliderEnabled(false);
			if (m_bottomCards.Count >= m_bottomSlots_Odd.Count)
				foreach (var slot in m_bottomSlots_Odd)
					slot.SetColliderEnabled(false);
		}

		public void AssignAndMoveCardToSlot(CardActor card, CardSlotActor slot, bool instant = false)
		{
			slot.MyCard = card;
			if (card != null)
			{
				//-.05f to avoid z-fighting. see "Z setting for Z fighting.txt"
				Vector3 destination = slot.transform.position + new Vector3(0, 0, -.05f);
				if (card.isCardFaceUp == false)
					card.MoveTo_WithRotationToFront(destination);//card on board is always face up
				if(instant == false)
					card.MoveMeTo(destination);
				else
					card.MoveMeTo_Instant(destination);
			}
		}

		/// <summary>
		///	SlotBoard keeps internal two rows of cards. they must be updated after card is added to board
		/// </summary>
		/// <param name="slot">slot the card is put into</param>
		public void AddCardToCardList(CardActor card, CardSlotActor slot)
		{
			//add card to cardList
			if (m_upperSlots_Even.Contains(slot) || m_upperSlots_Odd.Contains(slot))
			{
				Assert.IsFalse(m_upperCards.Contains(card), "upper cards already contain " + card);

				m_upperCards.Add(card);

				//order list of cards according to order of slots
				if (m_upperSlots_Even.Contains(slot))//don't optimise, happens once every 5s
					SetCardListFromSlots(m_upperCards, m_upperSlots_Even);
				else
					SetCardListFromSlots(m_upperCards, m_upperSlots_Odd);
			}
			else
			{
				Assert.IsFalse(m_bottomCards.Contains(card), "bottom cards already contain " + card);
				Assert.IsFalse(m_bottomSlots_Even.Contains(slot) == false &&
					m_bottomSlots_Odd.Contains(slot) == false, "bottom slots don't contain " + slot);

				m_bottomCards.Add(card);

				//order list of cards according to order of slots
				if (m_bottomSlots_Even.Contains(slot))//don't optimise, happens once every 5s
					SetCardListFromSlots(m_bottomCards, m_bottomSlots_Even);
				else
					SetCardListFromSlots(m_bottomCards, m_bottomSlots_Odd);
			}
		}

		public void RepositionAllCardsToCenterOfSlots()
		{
			//clear all cards from slots. not needed but clears data in editor
			foreach (var slot in m_upperSlots_Even)
				slot.MyCard = null;
			foreach (var slot in m_upperSlots_Odd)
				slot.MyCard = null;
			foreach (var slot in m_bottomSlots_Even)
				slot.MyCard = null;
			foreach (var slot in m_bottomSlots_Odd)
				slot.MyCard = null;

			//upper cards
			if (m_upperCards.Count > 0)
			{
				RepositionCardsToCenterOfSlots(
					m_upperCards,
					IsEven(m_upperCards.Count) ? m_upperSlots_Even : m_upperSlots_Odd
					);
			}
			//bottom cards
			if (m_bottomCards.Count > 0)
			{
				RepositionCardsToCenterOfSlots(
					m_bottomCards,
					IsEven(m_bottomCards.Count) ? m_bottomSlots_Even : m_bottomSlots_Odd
					);
			}
		}

		private void RepositionCardsToCenterOfSlots(List<CardActor> cardList, List<CardSlotActor> cardSlots)
		{
			string debugString = "";

			//define how many slots are free before card list
			int howManySlotsEmptyBeforeAndAfter = 0;
			var countOfSlots = cardSlots.Count;
			while (countOfSlots > cardList.Count)
			{
				countOfSlots -= 2;
				howManySlotsEmptyBeforeAndAfter++;
			}
			int howManyEmptySlotsBefore = howManySlotsEmptyBeforeAndAfter;

			//LogToDebug("howManyEmptySlotsBefore: " + howManyEmptySlotsBefore);
			//iterate through all card slots and assign cards, to first slots nulls, later cards, and after cards nulls
			for (int i = 0; i < cardSlots.Count; i++)
			{
				var slot = cardSlots[i];
				if (howManyEmptySlotsBefore > 0)
				{
					debugString += "[null] ";
					AssignAndMoveCardToSlot(null, slot);//slot is before cards, assign null
					howManyEmptySlotsBefore--;
				}
				else
				{
					int currentCardListIndex = i - howManySlotsEmptyBeforeAndAfter;
					if (currentCardListIndex < cardList.Count)
					{
						if (cardList[i - howManySlotsEmptyBeforeAndAfter] == null)
							debugString += "[null dividing cards] ";//card inside cards can be null, e.g. empty card that divides row during dragging
						else
							debugString += "[" + cardList[i - howManySlotsEmptyBeforeAndAfter].name + "] ";
						AssignAndMoveCardToSlot(cardList[i - howManySlotsEmptyBeforeAndAfter], slot, false);
					}
					else
					{
						debugString += "[null] ";
						AssignAndMoveCardToSlot(null, slot);//slot is after cards, assign null
					}
				}
			}

			//Debug.Log("reposition " + cardList.Count + " / " + cardSlots.Count + ": " + debugString);
		}

		/// <summary>
		/// sets list of cards with order acording to order of slots (leftmost to rightmost)
		/// </summary>
		private void SetCardListFromSlots(List<CardActor> cardList, List<CardSlotActor> cardSlots)
		{
			cardList.Clear();
			for (int i = 0; i < cardSlots.Count; i++)
			{
				var slot = cardSlots[i];
				if (slot.MyCard != null)
				{
					cardList.Add(slot.MyCard);
				}
			}

		}		
		
		public void InsertCardIntoSlot(CardActor insertedCard, CardSlotActor slotUnderCard)
		{
			//define which row for slots, and which row of cards
			List<CardActor> cardsFromRow;
			List<CardSlotActor> slotsFromRow;
			GetCardListSlotRow(slotUnderCard, out cardsFromRow, out slotsFromRow);

			//reposition cards in this row to check if slot under card is empty
			//if (cardsFromRow.Count == 0)//don't do, if done the first dragged card isn't assigned and moved to slot
			//	return;//no cards to reposition
			RepositionCardsToCenterOfSlots(
				cardsFromRow,
				slotsFromRow
				);
			if (slotUnderCard.MyCard == null)
			{
				//target slot empty, just assign card
				AssignAndMoveCardToSlot(insertedCard, slotUnderCard);
				return;
			}

			//if left slot free, move cards one left
			if (slotsFromRow[0].MyCard == null)
			{
				for (int i = 0; i < slotsFromRow.Count; i++)
				{
					var slot = slotsFromRow[i];
					if (slot != slotUnderCard)
					{
						AssignAndMoveCardToSlot(slotsFromRow[i + 1].MyCard, slot);//assign card on right to this slot
					}
					else
					{
						AssignAndMoveCardToSlot(insertedCard, slotUnderCard);
						break;
					}
				}
			}
			else
			{
				//right slots free, move cards one right
				Assert.IsFalse(slotsFromRow[slotsFromRow.Count - 1] != null, "player drags card into full slots, should be checked before");
				for (int i = slotsFromRow.Count - 1; i >= 0; i--)
				{
					var slot = slotsFromRow[i];
					if (slot != slotUnderCard)
					{
						AssignAndMoveCardToSlot(slotsFromRow[i - 1].MyCard, slot);//assign card on left to this slot
					}
					else
					{
						AssignAndMoveCardToSlot(insertedCard, slotUnderCard);
						break;
					}
				}
			}
		}

		/// <param name="cardList">card list for which given slot is</param>
		/// <param name="slotRow">slot row where given slot is</param>
		private void GetCardListSlotRow(CardSlotActor slot,
			out List<CardActor> cardList, out List<CardSlotActor> slotRow)
		{
			//Debug.Log("slot under card " + slotUnderCard.name);
			if (m_upperSlots_Even.Contains(slot))
			{
				cardList = m_upperCards;
				slotRow = m_upperSlots_Even;
				//Debug.Log("row that cards would be placed " + m_upperSlotsGO_Odd.name);
			}
			else if (m_upperSlots_Odd.Contains(slot))
			{
				cardList = m_upperCards;
				slotRow = m_upperSlots_Odd;
				//Debug.Log("row that cards would be placed " + m_upperSlotsGO_Even.name);
			}
			else if (m_bottomSlots_Even.Contains(slot))
			{
				cardList = m_bottomCards;
				slotRow = m_bottomSlots_Even;
				//Debug.Log("row that cards would be placed " + m_bottomSlotsGO_Odd.name);
			}
			else if (m_bottomSlots_Odd.Contains(slot))
			{
				cardList = m_bottomCards;
				slotRow = m_bottomSlots_Odd;
				//Debug.Log("row that cards would be placed " + m_bottomSlotsGO_Even.name);
			}
			else
			{
				Debug.Log("no slots contain " + slot, this.gameObject);
				cardList = m_upperCards;//return anything
				slotRow = m_upperSlots_Even;
			}
		}

		public bool IsSlotRowFull(CardSlotActor slotFromRow)
		{
			//define which row for slots, and which row of cards
			List<CardActor> cardsFromRow;
			List<CardSlotActor> slotsFromRow;
			GetCardListSlotRow(slotFromRow, out cardsFromRow, out slotsFromRow);

			if (cardsFromRow.Count >= slotsFromRow.Count)//+1 because we cound dragged card
			{
				return true;//row of slots full
			}

			return false;
		}

		private bool IsEven(int number)
		{
			return number % 2 == 0;
		}

		public bool IsCardOnBoard(CardActor card)
		{
			return m_upperCards.Contains(card) || m_bottomCards.Contains(card);
		}

		//------------------------------AI functions------------------------------------

		public void AI_AddToUpperCardList(CardActor card)
		{
			Assert.IsFalse(m_upperCards.Contains(card), "already contains");
			m_upperCards.Add(card);
		}

		public void AI_AddToBottomCardList(CardActor card)
		{
			Assert.IsFalse(m_bottomCards.Contains(card), "already contains");
			m_bottomCards.Add(card);
		}

		public void AI_GetData(out int upperCardCount, out int upperSlotsCount, 
			out int bottomCardsCount, out int bottomSlotsCount)
		{
			upperCardCount = m_upperCards.Count;
			upperSlotsCount = m_upperSlots_Even.Count > m_bottomSlots_Odd.Count ?
				m_upperSlots_Even.Count : m_bottomSlots_Odd.Count;//return higher slots count

			bottomCardsCount = m_bottomCards.Count;
			bottomSlotsCount = m_bottomSlots_Even.Count > m_bottomSlots_Odd.Count ?
				m_bottomSlots_Even.Count : m_bottomSlots_Odd.Count;//return higher slots count
		}

		public CardActor AI_GetRandomCard()
		{
			if (m_bottomCards.Count == 0)
				return m_upperCards[Random.Range(0, m_upperCards.Count)];
			if (m_upperCards.Count == 0)
				return m_bottomCards[Random.Range(0, m_bottomCards.Count)];

			if (Random.Range(0, 100) < 50)
				return m_upperCards[Random.Range(0, m_upperCards.Count)];
			else
				return m_bottomCards[Random.Range(0, m_bottomCards.Count)];
		}

		public int GetPointsOnBoard()
		{
			int returnedPoints = 0;
			foreach (var card in m_upperCards)
				returnedPoints += card.Health;
			foreach(var card in m_bottomCards)
				returnedPoints += card.Health;
			return returnedPoints;
		}

		public bool AreAllSlotsFull()
		{
			AI_GetData(out int upperCardCount, out int upperSlotsCount,
				out int bottomCardsCount, out int bottomSlotsCount);
			return upperCardCount >= upperSlotsCount && bottomCardsCount >= bottomSlotsCount;
		}

		public bool AreAllSlotsEmpty()
		{
			return m_upperCards.Count == 0 && m_bottomCards.Count == 0;
		}

		public void FlashAllCardsALittle()
		{
			foreach (var card in m_upperCards)
				card.FlashALittle();
			foreach (var card in m_bottomCards)
				card.FlashALittle();
		}

		public void RemoveCard(CardActor card)
		{
			if(m_upperCards.Contains(card))
			{
				m_upperCards.Remove(card);
				foreach (var slot in m_upperSlots_Even)
					if (slot.MyCard == card)
						slot.MyCard = null;
				foreach (var slot in m_upperSlots_Odd)
					if (slot.MyCard == card)
						slot.MyCard = null;
				return;
			}
			else if (m_bottomCards.Contains(card))
			{
				m_bottomCards.Remove(card);
				foreach (var slot in m_bottomSlots_Even)
					if (slot.MyCard == card)
						slot.MyCard = null;
				foreach (var slot in m_bottomSlots_Odd)
					if (slot.MyCard == card)
						slot.MyCard = null;
				return;
			}
			Debug.Log("remove card but it wasn't on board " + card.name, this.gameObject);
		}

		/// <summary>
		/// for doing effects
		/// todo perhaps SlotBoard should change it for every effect
		/// </summary>
		public List<CardActor> GetAllCardsFromBoard()
		{
			List <CardActor> returnedList = new List<CardActor>(m_upperCards);
			returnedList.AddRange(m_bottomCards);
			return returnedList;
		}

	}
}
