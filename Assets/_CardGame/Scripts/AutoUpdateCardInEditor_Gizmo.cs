using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGame
{
	/// <summary>
	/// Utility automatically changing sprite and sprite size from data, 
	/// for testing new data
	/// </summary>
	[ExecuteAlways]
	public class AutoUpdateCardInEditor_Gizmo : MonoBehaviour
	{
		[SerializeField] private CardActor m_card;

		[Header("Debug")]
		/*[DisplayAsString]*/ [SerializeField] private Vector3 m_posOfMyHealthText;

		private void Start()
		{
			if (Application.isPlaying)
				this.enabled = false;//make sure it's disabled in playMode (it shouldn't run as it runs when scene
			//changes, but make sure
		}

		private void Update()
		{
			ManualUpdate();
			m_posOfMyHealthText = m_card.PosOfHealthText;
		}

		//[Button("Update Now")] //Removed Odin
		private void ManualUpdate()
		{
			m_card.SetImageFromData();
		}
	}
}