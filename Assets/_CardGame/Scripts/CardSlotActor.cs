using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGame
{
	public class CardSlotActor : MonoBehaviour
	{
		private string m_initialName;
		[SerializeField] private Collider m_collider;

		[Header("Debug don't use")]
		[SerializeField] private CardActor m_card = null;//serialize field for debug

		public CardActor MyCard
		{
			get => m_card;
			set//setter not field
			{
				m_card = value;
#if UNITY_EDITOR
				if (value == null)
					this.name = m_initialName;
				else
					this.name = m_initialName + " [" + value.name + "]";
#endif
			}
		}


		void Start()
		{
			m_initialName = this.name;
		}

		public void SetColliderEnabled(bool colliderEnabled)
		{
			m_collider.gameObject.SetActive(colliderEnabled);
		}
	}
}