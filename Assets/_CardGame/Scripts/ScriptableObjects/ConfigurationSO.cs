using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGame
{
	[CreateAssetMenu(fileName = "Configuration", menuName = "ScriptableObjects/GameConfiguration")]
	public class ConfigurationSO : ScriptableObject
	{
		[Header("Game")]
		public int StartingCards = 4;

		[Header("UI")]
		public float TimeToLiveRisingText = 3.0f;
		public float SpeedRisingText = 1.0f;

		[Header("Tween")]
		public float CardMoveDuration = .3f;
		public float CardSelectTintDuration = .3f;
		public float CardRedFlashDuration = .5f;
		public float CardLittleFlashDuration = .7f;
		public float NewTurnTextDuration = 1.0f;
	}
}