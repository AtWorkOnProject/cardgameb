using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGame
{
	/// <summary>
	/// managed by <see cref="EffectManager"/>
	/// </summary>
	[CreateAssetMenu(fileName = "New Effect", menuName = "ScriptableObjects/Effect")]
	public class EffectSO : ScriptableObject
	{
		//[InfoBox("Select some effect checkboxes.", InfoMessageType.Warning, "HasNoEffectsSelected")]
		public EEffectTarget Target = EEffectTarget.EnemyCardOnBoard;

		//health
		[Tooltip("Heals / Damages chosen card.")]
		public bool ChangeHealth;
		//[ShowIf("ChangeHealth")]
		//[DetailedInfoBox("If negative, it's Damage.", "If negative, it's damage.\nIf positive, it's healing.")]
		//[GUIColor("GetColorForHealth")]
		public int HealthChange = -4;

		[Tooltip("when enemy is killed, it heals / damages itself")]
		public bool ChangeHealthWhenEnemyKilled;
		//[ShowIf("ChangeHealthWhenEnemyKilled")]
		//[DetailedInfoBox("Effect target not important.", "Effect target not important.\nCard will always act on itself.")]
		//[GUIColor("GetColorForHealthWhenEnemyKilled")]
		public int HealthChangeWhenEnemyKilled = 1;

		public bool NeedsPlayerToClick
		{
			get
			{
				return ChangeHealth == true;// !!! IMPORTANT !!! when you add effect, update if player needs to click any card 
			}
		}

		[Tooltip("when card is at hand at end of round, change its health")]
		public bool ChangeHealthInHandAtEndOfTurn;
		//[ShowIf("ChangeHealthInHandAtEndOfTurn")]
		//[DetailedInfoBox("Effect target not important.", "Effect target not important.\nCard will always act on itself.")]
		//[GUIColor("GetColorForHealthInHandAtEndOfTurn")]
		public int HealthChangeInHandEndOfTurn = -1;

		private Color GetColorForHealth()//make health greenish when positive
		{
			return HealthChange >= 0 ? new Color(.9f, 1.0f, .9f) : new Color(1.0f, .9f, .9f);
		}
		private Color GetColorForHealthWhenEnemyKilled()
		{ 
			return HealthChangeWhenEnemyKilled >= 0 ? new Color(.9f, 1.0f, .9f) : new Color(1.0f, .9f, .9f);
		}
		private bool HasNoEffectsSelected() //show warning if no effects selected
		{ 
			return !ChangeHealth && !ChangeHealthWhenEnemyKilled; 
		}
		private Color GetColorForHealthInHandAtEndOfTurn()
		{
			return HealthChangeInHandEndOfTurn >= 0 ? new Color(.9f, 1.0f, .9f) : new Color(1.0f, .9f, .9f);
		}

	}

	public enum EEffectTarget
	{
		EnemyCardOnBoard = 0,
		MyCardOnBoard = 1,
	}
}