using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CardGame
{
	/// <summary>
	/// data for <see cref="CardActor"/>
	/// </summary>
	[CreateAssetMenu(fileName = "New Card", menuName = "ScriptableObjects/Card")]
	public class CardDataSO : ScriptableObject
	{
		public int InitialHealth = 4;

		[Header("Visual")]
		public string Header = "New Card";
		[TextArea(1,15)]
		public string Description = "";
		public Sprite Sprite;
		public Color Tint = Color.white;
		public float SpriteScale = 1.0f;

		[Header("Effects")]
		public List<EffectSO> Effects;

		[Header("Documentation")]
		[TextArea(1,10)]
		public string Documentation;
	}
}