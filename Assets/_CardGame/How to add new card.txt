To Add a new card:
1. open scene "Game"
2. duplicate Dwarf card in Assets/_CardGame/Data/
3. in scene "Game", go to AIHand and select any card (preferably one of Base Cards)
4. assign your duplicated card to Data field in a card
5. change settings for your duplicated card (in Assets/_CardGame/Data). you should see your changes in the scene

Don't change prefab of cards. card objects are initialized at start() from Data field
